import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { Game } from '../Interfaces/game';
import { Picklist } from '../Interfaces/picklist';
import { User } from '../Interfaces/user';
import { GameService } from '../Services/game.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {
  games: Array<Game> = [];
  game: Game;
  showNewGame: boolean = false;
  closeResult = '';

  constructor(
    private _gameService: GameService,
    private modalService: NgbModal
  ) { }

  ngOnInit(): void {
    this._gameService.getGames()
    .subscribe(games => {
      this.games = games;
    }, err => {
      console.log(err);
    })
  }

  editGame(game: Game) {
    this.game = game;
  }

  deleteGame(game: Game) {
    this._gameService.deleteGame(game);
  }

  open(content, game?: Game) {
    console.log(game);
    if(game){
      this.game = game;
    }else{
      this.game = { gameId:(this.games.length + 1).toString(),name:"",startDate:new Date(),endDate:new Date(),picklists:new Array<Picklist>(),managers:new Array<User>()}
    }
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then(() => {
      if(game){
        this._gameService.updateGame(this.game);
      }
      else{
        this._gameService.addGame(this.game);
      }
      this.game = null;
    }, () => {
      this.game = null;
    });
  }

  getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
