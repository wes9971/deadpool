import { waitForAsync, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { Router, RouterModule } from '@angular/router';
// import { NgModuleFactory, Component, NgModule } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { routes } from './app-routing.module'
// import { Location } from '@angular/common';
import { AppComponent } from './app.component';
import { HomeModule } from './home/home.module';

beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
    declarations: [AppComponent],
    imports: [RouterTestingModule.withRoutes(routes), BrowserAnimationsModule, HomeModule]
    }).compileComponents();
}));