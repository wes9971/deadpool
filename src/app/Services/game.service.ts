import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { FakeDbService } from '../fake-db/fake-db.service';
import { Game } from '../Interfaces/game';
import { Picklist } from '../Interfaces/picklist';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  constructor(
    private _fakeDb: FakeDbService
  ) { }

  public getGames(): Observable<Array<Game>>{
    return of(this._fakeDb.games);
  }

  public deleteGame(game): Observable<any>{
    return of(this._fakeDb.deleteGame(game));
  }

  public updateGame(game): Observable<any>{
    return of(this._fakeDb.updateGame(game));
  }

  public addGame(game): Observable<any>{
    return of(this._fakeDb.addGame(game));
  }

  public addPicklistToGame(game: Game, picklist: Picklist){
    return of(this._fakeDb.addPicklistToGame(game, picklist));
  }
}
