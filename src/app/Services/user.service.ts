import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { FakeDbService } from '../fake-db/fake-db.service';
import { User } from '../Interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  
  public loggedInUser = null;

  constructor(
    private _fakeDb: FakeDbService
  ) { }

  public getUsers(): Observable<Array<User>>{
    return of(this._fakeDb.users);
  }

  public deleteUser(user): Observable<any>{
    return of(this._fakeDb.deleteUser(user));
  }

  public updateUser(user): Observable<any>{
    return of(this._fakeDb.updateUser(user));
  }

  public addUser(user): Observable<any>{
    return of(this._fakeDb.addUser(user));
  }
}
