import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { FakeDbService } from '../fake-db/fake-db.service';
import { Celebrity } from '../Interfaces/celebrity';

@Injectable({
  providedIn: 'root'
})
export class CelebrityService {

  constructor(
    private _fakeDb: FakeDbService
  ) { }

  public getCelebrities(): Observable<Array<Celebrity>>{
    return of(this._fakeDb.celebrities);
  }

  public deleteCelebrity(celebrity): Observable<any>{
    return of(this._fakeDb.deleteCelebrity(celebrity));
  }

  public updateCelebrity(celebrity): Observable<any>{
    return of(this._fakeDb.updateCelebrity(celebrity));
  }

  public addCelebrity(celebrity): Observable<any>{
    return of(this._fakeDb.addCelebrity(celebrity));
  }
}
