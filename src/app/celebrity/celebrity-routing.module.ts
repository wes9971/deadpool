import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CelebrityComponent } from './celebrity.component';

const routes: Routes = [
  { path: '', component: CelebrityComponent }, 
  { path: '/:id', loadChildren: () => import('./celebrity-card/celebrity-card.module').then(m => m.CelebrityCardModule) }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CelebrityRoutingModule { }
