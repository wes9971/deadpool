import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CelebrityCardRoutingModule } from './celebrity-card-routing.module';
import { CelebrityCardComponent } from './celebrity-card.component';


@NgModule({
  declarations: [CelebrityCardComponent],
  imports: [
    CommonModule,
    CelebrityCardRoutingModule
  ]
})
export class CelebrityCardModule { }
