import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CelebrityCardComponent } from './celebrity-card.component';

const routes: Routes = [{ path: '', component: CelebrityCardComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CelebrityCardRoutingModule { }
