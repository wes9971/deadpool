import { Component, OnInit } from '@angular/core';
import { Celebrity } from '../Interfaces/celebrity';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { CelebrityService } from '../Services/celebrity.service';

@Component({
  selector: 'app-celebrity',
  templateUrl: './celebrity.component.html',
  styleUrls: ['./celebrity.component.scss']
})
export class CelebrityComponent implements OnInit {
  celebrities: Array<Celebrity> = [];
  celebrity: Celebrity;
  showNewCelebrity: boolean = false;
  closeResult = '';

  constructor(
    private _celebrityService: CelebrityService,
    private modalService: NgbModal
  ) { }

  ngOnInit(): void {
    this._celebrityService.getCelebrities()
    .subscribe(celebs => {
      this.celebrities = celebs;
    }, err => {
      console.log(err);
    })
  }

  editCelebrity(celebrity: Celebrity) {
    this.celebrity = celebrity;
  }

  deleteCelebrity(celebrity: Celebrity) {
    this._celebrityService.deleteCelebrity(celebrity);
  }

  open(content, celeb?: Celebrity) {
    console.log(celeb);
    if(celeb){
      this.celebrity = celeb;
    }else{
      this.celebrity = { celebId:(this.celebrities.length + 1).toString(),fullName:"",firstName:"",lastName:"",nickname:"",occupation:""}
    }
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then(() => {
      this.celebrity.fullName = this.celebrity.firstName + (this.celebrity.nickname ? " \"" + this.celebrity.nickname + "\" ":" ") + this.celebrity.lastName
      if(celeb){
        this._celebrityService.updateCelebrity(this.celebrity);
      }
      else{
        this._celebrityService.addCelebrity(this.celebrity);
      }
      this.celebrity = null;
    }, () => {
      this.celebrity = null;
    });
  }

  getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
