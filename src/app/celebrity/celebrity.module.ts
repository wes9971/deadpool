import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CelebrityRoutingModule } from './celebrity-routing.module';
import { CelebrityComponent } from './celebrity.component';


@NgModule({
  declarations: [CelebrityComponent],
  imports: [
    CommonModule,
    CelebrityRoutingModule,
    FormsModule
  ]
})
export class CelebrityModule { }
