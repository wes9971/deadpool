import { Injectable } from '@angular/core';
import { Celebrity } from '../Interfaces/celebrity';
import { Game } from '../Interfaces/game';
import { Picklist } from '../Interfaces/picklist';
import { User } from '../Interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class FakeDbService {
  public celebrities: Array<Celebrity> = [];
  public games: Array<Game> = [];
  public users: Array<User> = [];

  constructor() {
    this.populateUsers();
    this.populateCelebrities();
    this.populateGames();
  }

  public addCelebrity(celebrity: Celebrity){
    this.celebrities.push(celebrity);
  }

  public deleteCelebrity(celebrity: Celebrity){
    this.celebrities.splice(this.celebrities.indexOf(celebrity), 1);
  }

  public updateCelebrity(celebrity: Celebrity){
    var foundIndex = this.celebrities.findIndex(x => x.celebId == celebrity.celebId);
    this.celebrities[foundIndex] = celebrity;
  }

  public addGame(game: Game){
    this.games.push(game);
  }

  public deleteGame(game: Game){
    this.games.splice(this.games.indexOf(game), 1);
  }

  public updateGame(game: Game){
    var foundIndex = this.games.findIndex(x => x.gameId == game.gameId);
    this.games[foundIndex] = game;
  }

  public populateCelebrities(){
    var celeb1: Celebrity = { celebId: "1", fullName: "Gary Busey", firstName: "Gary", lastName: "Busey", nickname: null, birthdate: "1944-06-29", deathdate: null, occupation: "Actor" }
    this.addCelebrity(celeb1);
    var celeb2: Celebrity = { celebId: "2", fullName: "Julianne Hough", firstName: "Julianne", lastName: "Hough", nickname: null, birthdate: "1988-07-20", deathdate: null, occupation: null }
    this.addCelebrity(celeb2);
  }

  public populateGames(){
    var game1: Game = { gameId: "1", name: "My Second Cool Game", startDate: new Date("2021-01-01"), endDate: new Date("2021-12-31"), picklists: new Array<Picklist>(), managers: new Array<User>() };
    var picklist1: Picklist = { listId: (game1.picklists.length+1).toString(), user: this.users[0], picks: [], createdDate: new Date(), lastModifiedDate: new Date() };
    game1.picklists.push(picklist1);
    this.games.push(game1);
    var game2: Game = { gameId: "2", name: "My First Cool Game", startDate: new Date("2020-01-01"), endDate: new Date("2020-12-31"), picklists: new Array<Picklist>(), managers: new Array<User>() };
    game2.picklists.push(picklist1);
    var picklist2: Picklist = { listId: (game1.picklists.length+1).toString(), user: this.users[1], picks: [], createdDate: new Date(), lastModifiedDate: new Date() };
    game2.picklists.push(picklist2);
    this.games.push(game2);
  }

  public populateUsers(){
    var user1: User = { userId: "1", username: "rmollenkamp", firstName: "Roy", lastName: "Mollenkamp", email: "rmollenkamp@gmail.com" }
    var user2: User = { userId: "2", username: "gbusey", firstName: "Gary", lastName: "Busey", email: "gbusey@crazy.com" }
    this.users.push(user1);
    this.users.push(user2);
  }

  public addPicklistToGame(game: Game, picklist: Picklist): any {
    this.games.find(x => x.gameId == game.gameId).picklists.push(picklist);
  }

  public addUser(user: User): any {
    this.users.push(user);
  }
  
  public updateUser(user: User): any {
    var foundIndex = this.users.findIndex(x => x.userId == user.userId);
    this.users[foundIndex] = user;
  }

  public deleteUser(user: User): any {
    this.users.splice(this.users.indexOf(user), 1);
  }
}
