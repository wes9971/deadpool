import { Component, OnInit } from '@angular/core';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { User } from '../Interfaces/user';
import { UserService } from '../Services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  users: Array<User> = [];
  user: User;
  showNewUser: boolean = false;
  closeResult = '';

  constructor(
    private _userService: UserService,
    private modalService: NgbModal
  ) { }

  ngOnInit(): void {
    this._userService.getUsers()
    .subscribe(users => {
      this.users = users;
    }, err => {
      console.log(err);
    })
  }

  editGame(user: User) {
    this.user = user;
  }

  deleteGame(user: User) {
    this._userService.deleteUser(user);
  }

  open(content, user?: User) {
    console.log(user);
    if(user){
      this.user = user;
    }else{
      this.user = { userId:(this.users.length + 1).toString(), username: "", firstName: "", lastName:"",  email: ""}
    }
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then(() => {
      if(user){
        this._userService.updateUser(this.user);
      }
      else{
        this._userService.addUser(this.user);
      }
      this.user = null;
    }, () => {
      this.user = null;
    });
  }

  getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
