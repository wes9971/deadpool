import { Celebrity } from "./celebrity";
import { Pick } from "./pick";
import { User } from "./user";

export interface Picklist {
    listId: string;
    user: User;
    picks: Array<Pick>;
    createdDate: Date;
    lastModifiedDate: Date;
}
