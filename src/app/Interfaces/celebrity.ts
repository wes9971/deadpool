import { importExpr } from "@angular/compiler/src/output/output_ast";

export interface Celebrity {
    celebId: string;
    fullName: string;
    firstName?: string;
    lastName?: string;
    nickname?: string;
    birthdate?: string;
    deathdate?: string;
    occupation?: string;
}

export class celeb implements Celebrity{
    celebId: string = "";
    fullName:string = "";
    firstName: string = "";
    lastName: string = "";
    nickname: string = "";
    birthdate: string = "";
    deathdate: string = "";
    occupation: string = "";

    constructor(){

    }
}