export interface User {
    userId: string;
    username: string;
    firstName: string;
    lastName: string;
    email: string;
}
