import { Celebrity } from "./celebrity";

export interface Pick {
    pickId: string;
    pickNumber: number;
    celebrity: Celebrity;
}
