import { Picklist } from "./picklist";
import { User } from "./user";

export interface Game {
    gameId: string;
    name: string;
    startDate: Date;
    endDate: Date;
    picklists: Array<Picklist>;
    managers: Array<User>;

}

export class Game implements Game{
    gameId: string = "";
    name: string = "";
    startDate: Date = new Date();
    endDate: Date = new Date();
    picklists: Array<Picklist> = new Array<Picklist>();
    managers: Array<User> = new Array<User>();

    constructor(){
        
    }
}
