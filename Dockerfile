FROM nginx:latest
COPY dist/Deadpool/ /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf